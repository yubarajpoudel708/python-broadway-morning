from django.http import HttpResponse
from django.shortcuts import render
import json

# Create your views here.
from finance.models import Pay


def index(request):
    return HttpResponse("I am finance page")


def show_all_payees(request):
    all_payees = Pay.objects.all()
    payeList = []
    for payee in all_payees:
        payeList.append(payee.toDict())
    print(len(all_payees))
    return HttpResponse(json.dumps(payeList))