from django.db import models

# Create your models here.
class Staff(models.Model):
    JOB_ROLES = (("Manager", "Manager"), ("Reception", "Reception"), ("Md", "Managing Director"))
    name = models.CharField(max_length=120, blank=False)
    position = models.CharField(choices = JOB_ROLES, max_length= 100, blank=False)
    image = models.FileField(upload_to="staff")

    def __str__(self):
        return self.name

class Pay(models.Model):
    payee = models.ForeignKey(Staff, on_delete= models.SET_NULL, blank=True, null=True)
    amount = models.DecimalField(max_digits= 5, decimal_places=2)
    pay_date = models.DateField(auto_created= True)
    remarks = models.TextField()
    publish = models.BooleanField(default= False)

    def __str__(self):
        return self.payee.name

    def toDict(self):
        return {'payee': self.payee.name, 'amount': str(self.amount)}

