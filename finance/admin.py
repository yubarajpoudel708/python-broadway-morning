from django.contrib import admin

# Register your models here.
from finance.models import Pay, Staff


@admin.register(Pay)
class PayAdmin(admin.ModelAdmin):
    list_display = ['payee', 'amount', 'remarks', 'pay_date']
    search_fields = ("payee", "remarks")
    list_filter = ("payee", "amount")
    list_per_page = 10

@admin.register(Staff)
class StaffAdmin(admin.ModelAdmin):
    list_display = ['name', 'position']
    search_fields = ("name", )
